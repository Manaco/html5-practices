﻿var txtInput;

function initialize() {
    for (var i = 0; i < 10; i++) {
        document.getElementById('btn' + i).addEventListener('click',
        numberClick, false);
    }

    txtInput = document.getElementById('txtInput');
    txtResult = document.getElementById('txtResult');

    document.getElementById('btnPlus').addEventListener('click', plusClick, false);
    document.getElementById('btnMinus').addEventListener('click', minusClick, false);
    document.getElementById('btnClearEntry').addEventListener('click', clearClick, false);
}

function numberClick() {
    txtInput.value = txtInput.value == '0' ?  this.innerText : txtInput.value + this.innerText;
}

function minusClick() {
    txtResult.value = Number(txtResult.value) - Number(txtInput.value);
    
}

function plusClick() {
    txtResult.value = Number(txtResult.value) + Number(txtInput.value);
}

function clearClick() {
    txtResult.value = Number(0);
    txtInput.value = Number(0);
}